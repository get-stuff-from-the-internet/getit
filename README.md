# Steps

1. fork repo
1. add variable for `RSS_URL` in your repo.
  1. it must be a base64 encoded version of your `RSS_URL` e.g. from showrss.info
1. if you set the repo to private: add a PAT and adjust the `--header "JOB-TOKEN: $CI_JOB_TOKEN"` to reflect your variable.
1. Schedule run every... N hours/minutes/etc.
1. pull the image: e.g. `docker pull registry.gitlab.com/get-stuff-from-the-internet/getit:Brooklyn_Nine_Nine_S07E13`
1. think about how to cleanup :p - https://docs.gitlab.com/ee/api/container_registry.html#delete-a-registry-repository-tag
