#!/bin/bash

usage() { echo "Usage: $0 [-r <REGISTRY>] [-p <PROJECT_ID>] [-i <REGISTRY_ID>] [-t <TARGET> (if unset will use $(pwd))]" 1>&2; exit 1; }
while getopts ":r:p:i:t:" o; do
  case "${o}" in
    r)
      REGISTRY="${OPTARG:-unset}"
      ;;
    p)
      PROJECT_ID="${OPTARG:-unset}"
      ;;
    i)
      REPO_ID="${OPTARG:-unset}"
      ;;
    t)
      TARGET="${OPTARG:-unset}"
      ;;
    *)
      usage
      ;;
  esac
done
shift $((OPTIND-1))

function check {
  if [[ "unset" == "${1:-unset}" ]]; then
    echo "$(date) - $2 is not set"
    usage
    exit 1
  fi
}

check "$TOKEN" "env var TOKEN"
check "$REGISTRY" "env var REGISTRY"
check "$PROJECT_ID" "env var PROJECT_ID"
check "$REPO_ID" "env var REPO_ID"

function get {
  check "$1" "Tag to get"
	echo "$(date) - Getting $1"

 if docker pull "$REGISTRY:$1"; then
   echo "$(date) - Successfully got $1"
 else
   echo "$(date) - Error getting $1"
   exit 1
 fi
}

function remove {
  check "$1" "Tag to delete"
  echo "$(date) - Removing $1"
 if curl --request DELETE --header "PRIVATE-TOKEN: $TOKEN" "https://gitlab.com/api/v4/projects/$PROJECT_ID/registry/repositories/$REPO_ID/tags/$1"; then
   echo "$(date) - Successfully deleted remote container: $1"
 else
   echo "$(date) - Error deleting remote container: $1"
   exit 1
 fi
 if docker image rm -f "$REGISTRY:$1"; then
   echo "$(date) - Successfully deleted local container: $1"
 else
   echo "$(date) - Error deleting local container: $1"
   exit 1
 fi
}

function unpack {
  check "$1" "image tag to unpack"
  if [[ "unset" == "${TARGET:-unset}" ]]; then
    echo "$(date) - $2 is not set; using current path: '$(pwd)'"
    TARGET=$(pwd) 
  fi
  mkdir "$TARGET/$1"
  id=$(docker create "$REGISTRY:$1")
  if docker cp "$id:/finished" "$TARGET/$1"; then
    echo "$(date) - Successfully extracted finished: $1"
  else
    echo "$(date) - Failed to extract finished from container: $1"
    exit 1
  fi
  find "$TARGET/$1/finished/" -type f -print0 | xargs -0 mv -t "$TARGET/$1"
  rm -R "$TARGET/$1/finished/"
  if docker rm "$id"; then
    echo "$(date) - Successfully deleted temporary container: $1"
  else
    echo "$(date) - Failed to delete temporary container: $1"
    exit 1
  fi
}


for tag in $(curl -s --output - --header "PRIVATE-TOKEN: $TOKEN" --location "https://gitlab.com/api/v4/projects/$PROJECT_ID/registry/repositories/$REPO_ID/tags" | jq -r '.[].name'); do
  get "$tag"
  unpack "$tag"
  remove "$tag"
done
